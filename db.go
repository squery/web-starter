package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Client struct {
	gorm.Model
	Email string
}

type DB struct {
	db *gorm.DB
}

func (db *DB) Close() {
	db.db.Close()
}

func OpenDB() *DB {
	d := conf.GetDB()
	connect := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v", d.Host, d.Port, d.User, d.DBName, d.Password)
	db, err := gorm.Open(d.Dialect, connect)
	check(err)
	db.AutoMigrate(&Client{})
	return &DB{db: db}
}
