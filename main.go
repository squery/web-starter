package main

import (
	"flag"
	// "fmt"
	"log"
	"os"
)

func main() {
	var configFile string
	var serveLogFile string
	var errorsLogFile string
	flag.StringVar(&configFile, "-config", "config/config.json", "set config file")
	flag.StringVar(&serveLogFile, "-serve-log", "log/serve.log", "set log file")
	flag.StringVar(&errorsLogFile, "-errors-log", "log/errors.log", "set log file")
	flag.Parse()

	serveFile, err := os.OpenFile(serveLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer serveFile.Close()

	errorsFile, err := os.OpenFile(errorsLogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer errorsFile.Close()

	StartLogger(serveFile, errorsFile)
	setErrOutput(errorsFile)

	ReadConf(configFile)

	s := NewServer()
	s.Start()
	defer s.Close()
}
