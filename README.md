
# Web-start

## Kit-starter for web-applicaton in Golang



### Get started

Replace default values on config file

    mkdir ProjectDir
    cd ProjectDir
    git clone https://gitlab.com/squery/web-start . 
    go mod init ProjectName
    go build


#### Server
Start server using port from config file

    s := NewServer()
    s.Start()

#### Errors
Errors handling work with weberr and loging error in log/errors.log. See [weberr](https://gitlab.com/squery/webrr)


#### Logger 
    
    Logger.Serve() //log.Println() in log/serve.log
    Logger.Errors() //log.Println() in log/errors.log


#### Config
Config file must be JSON Format. [Here](https://gitlab.com/squery/web-start/blob/master/config/config.json) minimal view


#### Flags
    --config 'config file path' default: 'config/config.json'
    --serve-log 'serve log file path' default: 'log/serve.log'
    --errors-log 'errors log file path' default: 'log/errors.log'

#### Using packages
- [Gorilla/mux](https://github.com/gorilla/mux)
- [GORM](https://github.com/jinzhu/gorm)
- [Webrr](https://gitlab.com/squery/webrr)
