package main

import (
	// "fmt"
	// "io/ioutil"
	"net/http"
)

func ErrorHandler(w http.ResponseWriter) {
	if r := recover(); r != nil {
		if err, ok := r.(error); ok {
			logErr(err)
			HTTPError(w, err)
		} else {
			panic(r)
		}
	}
}

func (s *server) FileHandler(filename string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

func (s *server) HelloHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})
}

func (s *server) LogMiddleWare(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer ErrorHandler(w)
		Logger.Serve(r.RequestURI, "|", r.Method, "|", r.RemoteAddr)
		next.ServeHTTP(w, r)
	})
}
