package main

import (
	"encoding/json"
	"io/ioutil"
)

var conf Configer

type DBConf struct {
	Dialect  string `json:"dialect"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	DBName   string `json:"db_name"`
	Password string `json:"password"`
}

type Configuration struct {
	Port string `json:"port"`

	Database DBConf `json:"database"`
}

type Configer interface {
	Get() Configuration
	GetDB() DBConf
}

func (c Configuration) Get() Configuration {
	return c
}

func (c Configuration) GetDB() DBConf {
	return c.Database
}

func ReadConf(path string) {
	bts, err := ioutil.ReadFile(path)
	check(err)

	c := &Configuration{}
	err = json.Unmarshal(bts, &c)
	check(err)

	conf = c
}
