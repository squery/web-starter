package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type server struct {
	db     *DB
	router *mux.Router
}

func NewServer() *server {
	s := server{
		db: OpenDB(),
	}
	s.route()
	return &s
}

func (s *server) Start() {
	c := conf.Get()
	log.Println("START SERVER")
	log.Fatal(http.ListenAndServe(":"+c.Port, s.router))
}

func (s *server) Close() {
	s.db.Close()
}

func (s *server) openDB() {
	s.db = OpenDB()
}

func (s *server) route() {
	r := mux.NewRouter()
	r.Handle("/", s.FileHandler("static/index.html"))
	r.Handle("/hello", s.HelloHandler())
	r.Use(s.LogMiddleWare)

	s.router = r
}
