package main

import (
	"io"
	"log"
)

var Logger logger

type logger struct {
	serve  *log.Logger
	errors *log.Logger
}

func StartLogger(serveLogFile, errorsLogFile io.Writer) {
	Logger = logger{
		serve:  log.New(serveLogFile, "", log.Ldate|log.Ltime),
		errors: log.New(errorsLogFile, "", log.Ldate|log.Ltime),
	}
	Logger.Serve("START SERVER")
	Logger.Errors("START SERVER")
}

func (l *logger) Serve(v ...interface{}) {
	l.serve.Println(v)
}

func (l *logger) Errors(v ...interface{}) {
	l.errors.Println(v)
}
