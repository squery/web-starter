package main

import (
	"errors"
	"io"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/squery/webrr"
)

func check(err error) {
	if err != nil {
		panic(webrr.Add(err))
	}
}

func checkWithMessage(err error, msg string) {
	if err != nil {
		panic(webrr.AddWithMessage(err, msg))
	}
}

func checkHTTP(err error, msg string, statusCode int) {
	if err != nil {
		panic(webrr.AddHTTPError(err, msg, statusCode))
	}
}

func checkTx(err error, tx *gorm.DB) {
	if err != nil {
		tx.Rollback()
		panic(webrr.Add(err))
	}
}

func HTTPError(w http.ResponseWriter, err error) {
	webrr.HTTPError(w, err)
}

func setErrOutput(w io.Writer) {
	webrr.SetOutput(w)
}

func logErr(err error) {
	webrr.Log(err)

}

func newErr(desc string) error {
	return webrr.Add(errors.New(desc))
}
